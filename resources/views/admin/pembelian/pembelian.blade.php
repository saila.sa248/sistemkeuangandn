@extends('admin.layouts.master')
@section('content')
<div class="page-title">
    <div class="title_left">
        <h3>DATA PEMBELIAN</h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <form method="get" action="{{ route('cari_pembelian') }}">
                <div class="input-group">
                    <input type="text" name="caripembelian" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="x_panel">
    <div class="x_title">
        <a href="{{ route('tambah_pembelian') }}">
            <h2><i class="fa fa-plus-square"></i> Tambah Data</h2>
        </a>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action text-center">
                <thead>
                    <tr class="headings">
                        <th class="column-title"> Id Pembelian </th>
                        <th class="column-title"> Tanggal Pembelian </th>
                        <th class="column-title"> Kode Suplier </th>
                        <th class="column-title"> Suplier </th>
                        <th class="column-title"> Nama Suplier </th>
                        <th class="column-title"> Total Pembelian </th>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($data as $d)
                    <tr class="even pointer">
                        <td class=" ">{{ $d->id_pembelian }}</td>
                        <td class=" ">{{ $d->tgl_beli }} </td>
                        <td class=" ">{{ $d->kd_suplier }}</td>
                        <td class=" ">{{ $d->perusahaan }}</td>
                        <td class=" ">{{ $d->nm_suplier }}</td>
                        <td class=" ">@currency($d->grand_total)</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection