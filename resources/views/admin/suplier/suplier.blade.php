@extends('admin.layouts.master')
@section('content')
<div class="page-title">
    <div class="title_left">
        <h3>DATA <small>SUPLIERS</small></h3>
    </div>


    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <form method="get" action="{{ route('cari_suplier') }}">
                <div class="input-group">
                    <input type="text" name="cari" value="{{ old('cari') }}" class="form-control"
                        placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="x_panel">
    <div class="x_title">
        <a href="{{ route('tambah_suplier') }}">
            <h2><i class="fa fa-plus-square"></i> Tambah Suplier</h2>
        </a>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action text-center">
                <thead>
                    <tr class="headings">
                        <th class="column-title"> Kode Suplier </th>
                        <th class="column-title"> Nama Suplier </th>
                        <th class="column-title"> Perusahaan </th>
                        <th class="column-title"> No Telepon </th>
                        <th class="column-title no-link last"><span class="nobr">Aksi</span>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($data as $d)
                    <tr class="even pointer">
                        <td class=" ">{{ $d->kd_suplier }}</td>
                        <td class=" ">{{ $d->nm_suplier }} </td>
                        <td class=" ">{{ $d->perusahaan }}</td>
                        <td class=" ">{{ $d->no_telp }}</td>
                        <td class=" last">
                            <div class="btn-group">
                                <a href="{{ route('edit_suplier', $d->kd_suplier) }}"
                                    class="btn btn-secondary btn-sm text-white" type="button">Ubah</a>
                                <a href="{{ route('delete_suplier', $d->kd_suplier) }}"
                                    class="btn btn-secondary btn-sm text-white" type="button">Hapus</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection